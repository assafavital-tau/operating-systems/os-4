#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <errno.h>
#include <string.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h> 
#include <signal.h>

#define BUFFER_LIMIT 4096
#define U_RND "/dev/urandom"

int key_fd, conn_fd, listen_fd;
pid_t pid;

void printError(const char* msg, int err)
{
	printf("[ERROR] %s: %s.\nExiting...\n", msg, strerror(err));
}

void genRandomKey(int key_fd, int length)
{
	// Open urandom
	int u_rnd_fd = open(U_RND, O_RDONLY);
	if(u_rnd_fd < 0)
	{
		printError("Could not open /dev/urandom", errno);
		exit(errno);
	}

	char urnd_buff[BUFFER_LIMIT];
	ssize_t bytes_written = 0, tmp;
	while(bytes_written < length)
	{
		// Read from urandom		
		if((tmp = read(u_rnd_fd, urnd_buff, BUFFER_LIMIT)) < 0)
		{
			printError("Could not read from /dev/urandom", errno);
			exit(errno);
		}

		// Write to key file
		if(write(key_fd, urnd_buff, BUFFER_LIMIT) < 0)
		{
			printError("Could not write to key file", errno);
			exit(errno);
		}

		bytes_written += tmp;
	}

	// Close urandom and key file
	if(close(u_rnd_fd) < 0)
	{
		printError("Could not close urandom", errno);
		exit(errno);
	}
	if(close(key_fd) < 0)
	{
		printError("Could not close key file", errno);
		exit(errno);
	}
}

void sigintHandler(int signum)
{
	// Cleanup
	if(key_fd) close(key_fd);
	if(pid == 0 && conn_fd) close(conn_fd);
	if(pid && listen_fd) close(listen_fd);

	exit(signum);
}

void xorFile(int socket_fd, const char* keypath)
{
	char input_buff[BUFFER_LIMIT], key_buff[BUFFER_LIMIT];
	ssize_t key_bytes, bytes_recvd, keyrd, k_i;
	int i;

	// Open key file
	key_fd = open(keypath, O_RDONLY);
	if(key_fd < 0)
	{
		printError("Could not open key file", errno);
		exit(errno);
	}

	do
	{
		// Read from client
		bytes_recvd = recv(socket_fd, input_buff, BUFFER_LIMIT, 0);
		if(bytes_recvd < 0)
		{
			printError("recv() failed", errno);
			exit(errno);
		}
		if(bytes_recvd == 0) break;

		key_bytes = 0;
		while(key_bytes < bytes_recvd)	
		{
			keyrd = read(key_fd, key_buff + (k_i % BUFFER_LIMIT), bytes_recvd - key_bytes);

			// Read failed
			if(keyrd < 0)
			{
				printError("Could not read key file", errno);
				exit(errno);
			}

			// Reached EOF
			if(keyrd == 0)
			{
				if(lseek(key_fd, SEEK_SET, 0) < 0)
				{
					printError("lseek() failed", errno);
					exit(errno);
				}
			}

			// Read succeeded
			key_bytes += keyrd;
			k_i += keyrd;
		}

		// XOR
		for(i = 0; i < bytes_recvd; i++) input_buff[i] = input_buff[i] ^ key_buff[i];

		// Send back to client
		if(send(socket_fd, input_buff, bytes_recvd, 0) < 0)
		{
			printError("send() failed", errno);
			exit(errno);
		}
	}
	while(1);

	// Cleanup
	if(close(key_fd) < 0)
	{
		printError("Could not close key file", errno);
		exit(errno);
	}
	if(close(socket_fd) < 0)
	{
		printError("Could not close socket", errno);
		exit(errno);
	}
}

int main(int argc, char** argv)
{
	/* * * * * * * COMMAND-LINE ARGUMENTS * * * * * * */

	// Validate number of arguments
	if(argc != 3 && argc != 4)
	{
		printf("Invalid arguments.\nExiting...\n");
		exit(-1);
	}

	// Assign arguments
	short port = atoi(argv[1]);
	char* key = argv[2];

	// Validate key file if KEYLEN is not given
	if(argc == 3)
	{
		struct stat64 st_key;
		if(stat64(key, &st_key) < 0)
		{
			printError("stat() failed", errno);
			exit(errno);
		}

		if(st_key.st_size <= 0)
		{
			printf("Key file is empty or invalid.\nExiting...\n");
			exit(-1);
		}
	}

	// Key file generation
	if(argc == 4)
	{
		int keylen = atoi(argv[3]);
		key_fd = open(key, O_RDWR | O_CREAT | O_TRUNC, 0777);
		if(key_fd < 0)
		{
			printError("Could not open key file", errno);
			exit(errno);
		}

		if(ftruncate(key_fd, keylen) < 0)
		{
			printError("Could not truncate key file", errno);
			exit(errno);
		}

		genRandomKey(key_fd, keylen);
	}

	/* * * * * * * REGISTER TCP SOCKET * * * * * * */

	listen_fd = 0;
	struct sockaddr_in serv_addr;

	// Create socket
	listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(listen_fd < 0)
	{
		printError("Could not create socket", errno);
		exit(errno);
	}

	// Create server struct
	memset(&serv_addr, '0', sizeof(serv_addr)); 
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(port); 

	// Bind
	if(bind(listen_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		printError("Bind Failed", errno);
		exit(errno);
    }

    // Listen
	if(listen(listen_fd, 10) < 0)
	{
		printError("Listen Failed", errno);
		exit(errno);
    }

	// Handle SIGINT
	struct sigaction sigint_action;
	sigint_action.sa_handler = sigintHandler;
	sigint_action.sa_flags = 0;
	if(sigaction(SIGINT, &sigint_action, NULL) != 0)
	{
		printError("Signal handle registration failed", errno);
		exit(errno);
	}    

    /* * * * * * * ACCEPTION LOOP * * * * * * */

    while(1)
    {
		conn_fd = accept(listen_fd, NULL, NULL);
		if(conn_fd < 0)
		{
			printError("Accept Failed", errno);
			exit(errno);
		}

		// Fork
		pid = fork();
		if(pid < 0) printError("Fork failed, but moving on", errno);

		// Child should exit loop
		if(pid == 0) break;

		if(close(conn_fd) < 0)
		{
			printError("Could not close conn_fd", errno);
			exit(errno);
		}
	}

	/* * * * * * * INTERACTION WITH CHILD * * * * * * */

	if(close(listen_fd) < 0)
	{
		printError("Could not close listen_fd", errno);
		exit(errno);
	}

	// Encryption
	xorFile(conn_fd, key);

	exit(0);
}