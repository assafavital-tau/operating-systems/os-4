#include <errno.h>
#include <string.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h> 

#define BUFFER_LIMIT 4096

void printError(const char* msg, int err)
{
	printf("[ERROR] %s: %s.\nExiting...\n", msg, strerror(err));
}

int main(int argc, char** argv)
{
	int in_fd, out_fd;

	/* * * * * * * COMMAND-LINE ARGUMENTS * * * * * * */

	// Validate number of arguments
	if(argc != 5)
	{
		printf("Invalid arguments.\nExiting...\n");
		exit(-1);
	}

	// Assign arguments
	char* ip = argv[1];
	short port = atoi(argv[2]);
	char* in_file = argv[3];
	char* out_file = argv[4];

	// Validate "IN" file
	in_fd = open(in_file, O_RDONLY);
	if(in_fd < 0)
	{
		printError("Could not open input file", errno);
		exit(errno);
	}

	// Validate "OUT" file
	out_fd = open(out_file, O_WRONLY | O_CREAT | O_TRUNC, 0777);
	if(out_fd < 0)
	{
		printError("Could not open output file", errno);
		exit(errno);
	}

	/* * * * * * * CONNECTION ESTABLISHMENT * * * * * * */

	int socket_fd;
	struct sockaddr_in serv_addr;

	// Create socket
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_fd < 0)
	{
		printError("Could not create socket", errno);
		exit(errno);
	}

	// Create server struct
	memset(&serv_addr, '0', sizeof(serv_addr)); 
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	serv_addr.sin_addr.s_addr = inet_addr(ip);

	// Create connection
	if(connect(socket_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		printError("Connection failed", errno);
		exit(errno);
	}

	/* * * * * * * INTERACTION WITH SERVER * * * * * * */

	char input_buff[BUFFER_LIMIT], output_buff[BUFFER_LIMIT];
	ssize_t bytes_read, bytes_recvd, bytes_written, tmp_wrt;

	do
	{
		// Read from file
		bytes_read = read(in_fd, input_buff, BUFFER_LIMIT);
		if(bytes_read == 0) break;
		if(bytes_read < 0)
		{
			printError("Could not read from input file", errno);
			exit(errno);
		}

		// Send packet to server
		if(send(socket_fd, input_buff, BUFFER_LIMIT, 0) < 0)
		{
			printError("send() failed", errno);
			exit(errno);
		}

		// Receive packet from server
		bytes_recvd = recv(socket_fd, output_buff, BUFFER_LIMIT, 0);
		if(bytes_recvd < 0)
		{
			printError("recv() failed", errno);
			exit(errno);
		}

		// Write to output file
		bytes_written = 0;
		while(bytes_written < bytes_recvd)
		{
			tmp_wrt = write(out_fd, output_buff + bytes_written, bytes_recvd - bytes_written);
			if(tmp_wrt < 0)
			{
				printError("write() failed", errno);
				exit(errno);
			}

			bytes_written += tmp_wrt;
		}
	}
	while(1);

	/* * * * * * * DISCONNECTION * * * * * * */

	// Close I/O files
	if(close(in_fd) < 0) printError("Could not close input file", errno);
	if(close(out_fd) < 0) printError("Could not close output file", errno);

	// Close socket
	if(close(socket_fd) < 0) printError("Could not close socket", errno);

	exit(0);
}